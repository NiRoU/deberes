
import XCTest

// Se puede tener constructores de diferentes piezas con  procesos parecidos pero se los puede llamar de diferentes partes
// Por ejemplo costruir la casa de piedra madera o cristal
// Pero para poder llamarlas a todas necesito de una clase abstracta que se llamará director
// Esta clase es buena para llamar los pasos para construir piezas en un solo lugar y reutilizarlas desde allí.



// la interface Builder especifica los metodos para la creacion de diferentes parte de los Objetos

protocol Builder {
    
    func produceContrucParedes()
    func produceConstrucTecho()
    func produceConstrucPuertas()
    
}
/*
Las clases constructoras concretas su implementas luego de la interfaz constructora
 en donde implementas concretamente la construccion de caada objeto
*/



class ConstructorConcretoCasaMadera: Builder {
    
    /* Una instancia de constructor nueva debe contener un objeto de producto en blanco, que se
    utiliza  en un ensamblaje adicional. */
    
    private var producto = ProductoCasaMadera ( )
    
    func reset() {
        producto = ProductoCasaMadera ( )
    }
    
    /// Todos los pasos de producción funcionan con la misma instancia de producto.
    func produceContrucParedes() {
        producto.add(part: "Paredes Madera")
    }
    
    func produceConstrucTecho() {
        producto.add(part: "Techo Madera")
    }
    
    func produceConstrucPuertas() {
        producto.add(part: "Puertas Madera")
    }
    
    func devolverProducto ( ) -> ProductoCasaMadera {
        let  resultado  =  self.producto
        reset ( )
        return resultado
    }

}


/// El Director solo es responsable de ejecutar los pasos de construcción en un
/// secuencia particular. Es útil cuando se producen productos según un
/// orden o configuración específica. En sentido estricto, la clase Director es
/// opcional, ya que el cliente puede controlar los constructores directamente.

class  Director  {
    
    private  var  builder : Builder?
    
    /// El Director trabaja con cualquier instancia de constructor que el código del cliente le pasa
    ///. De esta manera, el código del cliente puede alterar el tipo final del producto recién ensamblado///.
    func update (builder : Builder)  {
    self.builder  =  builder
}

/// El Director puede construir varias variaciones de producto usando los mismos
/// pasos de construcción.
func  buildMinimalViableProduct ( )  {
    builder? .produceContrucParedes ( )
}

func  buildFullFeaturedProduct ( )  {
    builder? .produceContrucParedes ( )
    builder? .produceConstrucTecho ( )
    builder? .produceConstrucPuertas ( )
}
}

class ProductoCasaMadera {
    
    private var partes = [String]()
    
    func add(part: String) {
        self.partes.append(part)
    }
    
    func listParts() -> String {
        return "Partes de la Casa: " + partes.joined(separator: ", ") + "\n"
    }
}



class Client {
    // ...
    static func someClientCode(director: Director) {
        let builder = ConstructorConcretoCasaMadera()
        director.update(builder: builder)
        
        print("Producto estandar Basico:")
        director.buildMinimalViableProduct()
        print(builder.devolverProducto().listParts())
        
        print("Producto completo:")
        director.buildFullFeaturedProduct()
        print(builder.devolverProducto().listParts())
        
        // Remember, the Builder pattern can be used without a Director class.
        print("Producto personalizado:")
        builder.produceContrucParedes()
        builder.produceConstrucTecho()
        print(builder.devolverProducto().listParts())
    }
    // ...
}

class BuilderConceptual : XCTestCase {
    
    func testBuilderConceptual() {
        var director = Director();
        Client.someClientCode(director : director)
    }
}
