import UIKit


// El patron de desisenio Bridge divide la lógica empresarial o la clase enorme en jerarquías de clases separadas que se pueden desarrollar de forma independiente.


import XCTest

class AudioCommunicationsDevice {
    func sendAudio() {
    }
}

class CellPhone: AudioCommunicationsDevice {
    override func sendAudio() {
        // Implement function in subclasses
    }
}

class WalkieTalkie: AudioCommunicationsDevice {
    override func sendAudio() {
        // Implement function in subclasses
    }
}

class SecureCellPhone: CellPhone {
    override func sendAudio() {
        // Send encrypted audio
    }
}

class SecureWalkieTalkie: WalkieTalkie {
    override func sendAudio() {
        // Send encrypted audio
    }
}

class PlainAudioCellPhone: CellPhone {
    override func sendAudio() {
        // Send unencrypted audio
    }
}

class PlainAudioWalkieTalkie: WalkieTalkie {
    override func sendAudio() {
        // Send unencrypted audio
    }
}

// New Device
class LandlinePhone: AudioCommunicationsDevice {
    override func sendAudio() {
        // Implement function in subclasses
    }
}

// Audio Types
class SecureLandlinePhone: LandlinePhone {
    override func sendAudio() {
        // Send encrypted audio
    }
}

class PlainAudioLandlinePhone: LandlinePhone {
    override func sendAudio() {
        // Send plain audio
    }
}


protocol AudioHandling {
    func handle(audio: Audio) -> Audio
}

class AudioEncryptor: AudioHandling {
    func handle(audio: Audio) -> Audio {
        // Encrypt and return Audio
    }
}

class PlainAudioHandler: AudioHandling {
    func handle(audio: Audio) -> Audio {
        // return default audio
    }
}

