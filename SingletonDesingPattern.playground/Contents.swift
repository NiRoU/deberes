
/*
 Singleton es un patr[on de disenio que se recomienda usarlo cuando se tiene un solo objeto del cual pidan muchas instancias los clientes , asi como en las bases de datos.
 
 
 Se debe usar cuando se necesite un control estricto de las variables globales
 */



import XCTest


// se define una variable global


// se defina la clase accountmanager
class AccountManager {
    static let sharedInstance = AccountManager()
    
    var userInfo = (ID: "nirouthedev", Password: 01036343984)
    
    // Networking: communicating server
    func network() {
        // get everything
    }
    //El private init previene que se inicialice fuera de la clase
    private init() { }
}

//si esto se instancia en diferentes viewcontrollers solo se tiene acceso a un unico AccountManager()

//primer viewcontroller
print(AccountManager.sharedInstance.userInfo)
//segundo viewcontroller
AccountManager.sharedInstance.userInfo.ID = "bob"
//tercero viewcontroller
print(AccountManager.sharedInstance.userInfo.ID)

