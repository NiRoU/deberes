
//Las subclases pueden tener una sola clase padre. En la mayoría de los idiomas, la herencia no permite que una clase herede los comportamientos de varias clases al mismo tiempo.

// ENtonces en lugar de la herencia se utiliza la composicion ya que se hace mas facil cambiarlos en tiempo de ejecucion


// Entonces se entiende al patron decorador como la creacion de un envoltorio para una clase, una interfaz entonces esto le permitira usar el comportamiento combinado de todos los envoltorios,

//El patrón Decorador se usa cuando se necesite asignar comportamientos adicionales a los objetos en tiempo de ejecución sin romper el código que usa estos objetos.


//  Puedes combinar varios comportamientos envolviendo un objeto en varios decoradores.


import UIKit

import XCTest


//Interfaz
protocol Hamburguesa: CustomStringConvertible {
    
    var costo : Double { get }
    
    var descripcion : String { get }
}



//CLASES

class HamburguesaTocino : Hamburguesa {
    var description: String = ""
    
   
    
    var costo: Double {
        get {
            return 3.99
        }
    }
    
    var descripcion: String {
        get {
            return "Hamburguesa con tocino"
        }
    }

}


class HamburguesDobleQueso: Hamburguesa {
    var description: String = ""
    

    
    
    var costo: Double {
        get {
            return 4.50
        }
    }
    
    var descripcion: String {
        get {
            return "Hamburguesa con doble queso"
        }
    }
    
}


class HamburguesaTripaMishqui : Hamburguesa {
    var description: String = ""
    

    
    var costo: Double {
        get {
            return 4.99
        }
    }
    
    var descripcion: String {
        get {
            return "Hamburguesa con Tripa Mishqui"
        }
    }
    
}


//Clase Decorador

class HamburguesaDecorador : Hamburguesa{
    var description: String = ""
    
   
    
    
    var costo: Double {
        get {
            return mbInstance.costo
        }
    }
    
    var descripcion: String {
        get {
            return mbInstance.descripcion
        }
    }
    
    let mbInstance : Hamburguesa
    
    required init(hamburguesa: Hamburguesa) {
        self.mbInstance = hamburguesa
    }
}


final class HamburguesaAddon : HamburguesaDecorador {
    
    override var costo: Double {
        get {
            return mbInstance.costo + 0.35
        }
    }
    
    override var descripcion: String {
        get {
            return mbInstance.descripcion + ", COla"
        }
    }
    
    required init(hamburguesa: Hamburguesa) {
        super.init(hamburguesa: hamburguesa)
    }
    
}



var hamburguesa: Hamburguesa = HamburguesDobleQueso()
print("Costo : £\(hamburguesa.costo), Descripcion: \(hamburguesa.descripcion)")
hamburguesa = HamburguesaAddon(hamburguesa: <#T##Hamburguesa#>)
print("Costo : £\(hamburguesa.costo), Descripcion: \(hamburguesa.descripcion)")
hamburguesa = HamburguesaAddon(hamburguesa: <#T##Hamburguesa#>)
print("Costo : £\(hamburguesa.costo), Descripcion: \(hamburguesa.descripcion)")

